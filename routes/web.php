<?php

use App\Http\Controllers\BackendController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ContactSubjectController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectListController;
use App\Http\Controllers\ProjectTagController;
use App\Http\Controllers\TagTypeController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;

Route::get('/', [PageController::class, 'index'])->name('home');
Route::prefix('portfolio')->group(function () {
    Route::get('/', [ProjectController::class, 'index'])->name('projects');
    Route::get('/project/{id}', [ProjectController::class, 'show'])->name('project.details');
    Route::get('/category/{category}', [CategoryController::class, 'index'])->name('projects.tagged');
});

Route::prefix('contact')->group(function () {
    Route::get('/', [ContactController::class, 'index'])->name('contact');
    Route::post('/store', [ContactController::class, 'store'])->name('contact.store');
});

Route::prefix('WB')->group(function () {

    Route::middleware(['auth', 'verified'])->group(function () {

        // Dashboard
        Route::get('/', [PageController::class, 'wbIndex'])->name('dashboard');
        Route::post('/', [ContentController::class, 'update'])->name('content.update');

        // Projects
        Route::prefix('projects')->group(function () {
            
            Route::get('/', [ProjectController::class, 'wbIndex'])->name('wb.project.index');

            Route::prefix('project')->group(function () {
                Route::get('/new', [ProjectController::class, 'create'])->name('wb.project.new');
                Route::get('/{id}', [ProjectController::class, 'wbShow'])->name('wb.project.details');
                Route::post('/store', [ProjectController::class, 'store'])->name('wb.project.store');
                Route::post('/edit/{id}', [ProjectController::class, 'edit'])->name('wb.project.edit');
                Route::delete('/delete/{id}', [ProjectController::class, 'delete'])->name('wb.project.delete');
            });
        });
        
        // ProjectLists
        Route::prefix('project-lists')->group(function () {
            Route::get('/', [ProjectListController::class, 'wbIndex'])->name('wb.list.index');
            Route::get('/new', [ProjectListController::class, 'create'])->name('wb.list.new');
            Route::get('/{id}', [ProjectListController::class, 'wbShow'])->name('wb.list.details');
            Route::post('/store', [ProjectListController::class, 'store'])->name('wb.list.store');
            Route::post('/edit/{id}', [ProjectListController::class, 'edit'])->name('wb.list.edit');
            Route::delete('/delete/{id}', [ProjectListController::class, 'delete'])->name('wb.list.delete');
        });

        // Contacts
        Route::prefix('contacts')->group(function () {
            Route::get('/', [ContactController::class, 'wbIndex'])->name('wb.contact.index');
            Route::get('/{id}', [ContactController::class, 'wbShow'])->name('wb.contact.details');
            Route::post('/edit/{id}', [ContactController::class, 'edit'])->name('wb.contact.edit');
            Route::delete('/delete/{id}', [ContactController::class, 'delete'])->name('wb.contact.delete');
        });

        Route::prefix('contact-subjects')->group(function () {
            Route::get('/', [ContactSubjectController::class, 'wbIndex'])->name('wb.subject.index');
            Route::get('/new', [ContactSubjectController::class, 'create'])->name('wb.subject.new');
            Route::get('/{id}', [ContactSubjectController::class, 'wbShow'])->name('wb.subject.details');
            Route::post('/store', [ContactSubjectController::class, 'store'])->name('wb.subject.store');
            Route::post('/edit/{id}', [ContactSubjectController::class, 'edit'])->name('wb.subject.edit');
            Route::delete('/delete/{id}', [ContactSubjectController::class, 'delete'])->name('wb.subject.delete');
        });

        // ProjectTags
        Route::prefix('project-tags')->group(function () {
            Route::get('/{tag}', [ProjectTagController::class, 'wbIndex'])->name('wb.tag.index');
            Route::get('/{tag}/new', [ProjectTagController::class, 'create'])->name('wb.tag.new');
            Route::get('/{tag}/details/{id}', [ProjectTagController::class, 'wbShow'])->name('wb.tag.details');
            Route::post('/store', [ProjectTagController::class, 'store'])->name('wb.tag.store');
            Route::post('/edit/{id}', [ProjectTagController::class, 'edit'])->name('wb.tag.edit');
            Route::delete('/delete/{id}', [ProjectTagController::class, 'delete'])->name('wb.tag.delete');
        });
        
        // ProjectTagsTypes
        Route::prefix('tag-types')->group(function () {
            Route::get('/', [TagTypeController::class, 'wbIndex'])->name('wb.tagtype.index');
            Route::get('/new', [TagTypeController::class, 'create'])->name('wb.tagtype.new');
            Route::get('/{id}', [TagTypeController::class, 'wbShow'])->name('wb.tagtype.details');
            Route::post('/store', [TagTypeController::class, 'store'])->name('wb.tagtype.store');
            Route::post('/edit/{id}', [TagTypeController::class, 'edit'])->name('wb.tagtype.edit');
            Route::delete('/delete/{id}', [TagTypeController::class, 'delete'])->name('wb.tagtype.delete');
        });

        Route::delete('/images/remove/{id}', [ImageController::class, 'removeImage'])->name('wb.images.remove');
    });
});

require __DIR__.'/auth.php';
