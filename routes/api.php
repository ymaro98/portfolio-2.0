<?php

use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContactSubjectController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectTagController;
use App\Http\Controllers\TagTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('projects')->group(function () {
    Route::get('/all', [ProjectController::class, 'allProjects'])->name('api.projects.all');
    Route::get('/taggable', [ProjectController::class, 'projectsTaggable'])->name('api.projects.taggable');
    Route::get('/featured/{slug}', [ProjectController::class, 'featuredProjects'])->name('api.projects.featured');
});

Route::prefix('tags')->group(function () {
    Route::get('/all', [ProjectTagController::class, 'allTags'])->name('api.tags.all');
    Route::get('/type/category', [ProjectTagController::class, 'allCategoryTags'])->name('api.tags.category');
    Route::get('/type/technology', [ProjectTagController::class, 'allTechnologyTags'])->name('api.tags.technology');
    Route::get('/type/{id}', [ProjectTagController::class, 'tagsById'])->name('api.tags.type');
});

Route::prefix('contact-subjects')->group(function () {
    Route::get('/', [ContactSubjectController::class, 'allSubjects'])->name('api.subjects.all');
});

Route::prefix('tags-types')->group(function () {
    Route::get('/all', [TagTypeController::class, 'allTypes'])->name('api.tagtypes.all');
});

Route::get('/contacts/latest', [ContactController::class, 'getMessages'])->name('api.contacts.latest');



