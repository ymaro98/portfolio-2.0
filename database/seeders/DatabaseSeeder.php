<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(TagTypeSeeder::class);
        // $this->call(ProjectTagSeeder::class);
        // $this->call(ProjectSeeder::class);
        $this->call(TaggableSeeder::class);
    }
}
