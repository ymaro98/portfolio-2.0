<?php

namespace Database\Seeders;

use App\Models\ProjectTaggable;
use Illuminate\Database\Seeder;

class TaggableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectTaggable::factory()->times(30)->create();
    }
}
