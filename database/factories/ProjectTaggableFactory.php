<?php

namespace Database\Factories;

use App\Models\ProjectTaggable;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectTaggableFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectTaggable::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'project_id' => rand(1, 12),
            'tag_id' => rand(1, 20),
        ];
    }
}
