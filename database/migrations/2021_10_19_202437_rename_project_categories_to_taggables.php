<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameProjectCategoriesToTaggables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::rename('project_categories', 'project_tags');
        Schema::table('taggables', function (Blueprint $table) {
            $table->unsignedBigInteger('tag_id')->index();
            $table->foreign('tag_id')
                ->references('id')
                ->on('project_tags')
            ;
        });
        Schema::table('project_tags', function (Blueprint $table) {

            $table->unsignedBigInteger('tag_type_id')->index();
            $table->foreign('tag_type_id')
                ->references('id')
                ->on('tag_types')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_tags', function (Blueprint $table) {
            $table->dropForeign(['taggable_type_id']);
            $table->dropColumn('taggable_type_id');
        });

        Schema::table('taggables', function (Blueprint $table) {
            $table->dropForeign(['tag_id']);
            $table->dropColumn('tag_id');
        });

        Schema::rename('project_tags', 'project_categories');
        Schema::dropIfExists('tag_types');
    }
}
