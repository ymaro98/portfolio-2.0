<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->text('home_intro_content');
            $table->string('home_intro_img');
            $table->string('about_me_title');
            $table->text('about_me_content');
            $table->string('about_me_img');
            $table->unsignedBigInteger('featured_list_id')->index();
            $table->foreign('featured_list_id')
                ->references('id')
                ->on('project_lists')
            ;
            $table->string('portfolio_intro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
