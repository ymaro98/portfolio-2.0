<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\ProjectList;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PageController extends Controller
{
    public function index(){
        $content = Content::with('projectList')->first();
        return Inertia::render('Frontend/Home' , [
            "content" => $content,
        ]);
    }

    public function wbIndex(){
        $content = Content::first();
        $projectList = ProjectList::all();
        return Inertia::render('Backend/Dashboard', [
            "content" => $content,
            "projectLists" => $projectList,
        ]);
    }
}
