<?php

namespace App\Http\Controllers;

use App\Models\ProjectList;
use App\Models\ProjectListItem;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProjectListController extends Controller
{

    public function wbIndex()
    {
        $projectLists = ProjectList::latest()->paginate(6);
        return Inertia::render('Backend/ProjectLists/Index', [
            'projectLists' => $projectLists 
        ]);
    }

    public function wbShow($id){
        $projectList = ProjectList::whereId($id)->with('projects')->first();
        return Inertia::render('Backend/ProjectLists/Detail', [
            'projectList' => $projectList
        ]);
    }

    public function create(){
        $newProjectListId = ProjectList::all()->last()->id ?? 0;
        return Inertia::render('Backend/ProjectLists/New', [
            'newProjectListId' => $newProjectListId + 1
        ]);
    }

    public function store(Request $request){
        ProjectList::create($request->all());
        return redirect()->back();
    }

    public function edit(Request $request){
        
        $projectList_id = $request->id;
        $projectList = ProjectList::find($projectList_id);
        $projectList->update($request->all());
        $projects = $request->projects;
        $this->storeProjectListItems($projectList_id, $projects);
  
        return redirect()->back();
    }

    public function storeProjectListItems($id, $items){
        $project = ProjectList::where('id', $id)->first();
        $project->listItems()->delete();
        foreach ($items as $item) {
            $item = ProjectListItem::create([
                "project_list_id" => $id,
                "project_id" => $item['id']
            ]);
        }
    }
    
    public function delete(Request $request){

        $projectList_id = $request->id;
        $projectList = ProjectList::find($projectList_id);
        $projectList->delete();

        return redirect()->route('wb.list.index');
    }
}
