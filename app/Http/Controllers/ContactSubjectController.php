<?php

namespace App\Http\Controllers;

use App\Models\ContactSubject;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ContactSubjectController extends Controller
{
    public function wbIndex()
    {
        $subjects = ContactSubject::latest()->paginate(6);
        return Inertia::render('Backend/ContactSubjects/Index', [
            'subjects' => $subjects 
        ]);
    }

    public function wbShow($id){
        $subject = ContactSubject::whereId($id)->first();
        return Inertia::render('Backend/ContactSubjects/Detail', [
            'subject' => $subject
        ]);
    }

    public function create(){
        $newContactSubjectId = ContactSubject::all()->last()->id ?? 0;
        return Inertia::render('Backend/ContactSubjects/New', [
            'newContactSubjectId' => $newContactSubjectId + 1 
        ]);
    }

    public function store(Request $request){

        ContactSubject::create($request->all());
        return redirect()->back();
    }

    public function edit(Request $request){

        $contactSubject_id = $request->id;
        $contactSubject = ContactSubject::find($contactSubject_id);
        $contactSubject->update($request->all());
 
        return redirect()->back();
    }

    public function delete(Request $request){

        $contactSubject_id = $request->id;
        $contactSubject = ContactSubject::find($contactSubject_id);
        $contactSubject->delete();

        return redirect()->route('wb.subject.index');
    }

    public function allSubjects(){
        $subjects = ContactSubject::all();
        return $subjects;
    }
}
