<?php

namespace App\Http\Controllers;

use App\Mail\NewContact;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;

class ContactController extends Controller
{
    public function index(){
        return Inertia::render('Frontend/Contacts/Index');
    }

    public function wbIndex(){
        $contacts = Contact::with('subject')->oldest()->paginate(6);
        return Inertia::render('Backend/Contacts/Index', [
            'contacts' => $contacts 
        ]);
    }
    
    public function wbShow($id){
        $contact = Contact::whereId($id)->with('subject')->first();
        return Inertia::render('Backend/Contacts/Detail', [
            'contact' => $contact
        ]);
    }

    public function edit(Request $request){

        $contact_id = $request->id;
        $contact = Contact::find($contact_id);
        $contact->update($request->except('subject'));
 
        return redirect()->back();
    }

    public static function getMessages(Request $request){
        $limit = $request->limit ?? 3;
        $contacts_q = Contact::with('subject')->isNew()->oldest();
        $contacts_q->paginate($limit);
        $contacts = $contacts_q->get();
        return  $contacts;
    }

    public function store(Request $request){
        $contact = Contact::create($request->all());
        Mail::to(config('mail.from.address'))->send(new NewContact($contact));
        return redirect()->back();
    }

    public function delete(Request $request){

        $contact_id = $request->id;
        $contact = Contact::find($contact_id);
        $contact->delete();

        return redirect()->route('wb.contact.index');
    }
}
