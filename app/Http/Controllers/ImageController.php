<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public $imagePath;

    public function __construct()
    {
        $this->imagePath = '/public/images/projects/';
        $this->bgType = 'bg';
        $this->galleryType = 'gallery';
    }

    public function storeProjectImage($request, $project){
        $path = $request->file('image')->store($this->imagePath . $request->input('prefix'));
        
        $srcPath = '/storage/'. $path;
        $src = str_replace('public/', '', $srcPath);

        $oldImage = $project->bgImage();
        if($oldImage){
            $oldImage->delete();
        }
        
        $project->images()->save(
            Image::make([
                'path' => $src, 
                'real_path' => $path, 
                'type' => $this->bgType
            ])
        );
    }

    public function storeProjectImageMultiple($request, $project){

        $images = $request->file('images');
        foreach ($images as $image) {
            $path = $image->store($this->imagePath . $request->input('prefix'));
        
            $srcPath = '/storage/'. $path;
            $src = str_replace('public/', '', $srcPath);
    
            $project->images()->save(
                Image::make([
                    'path' => $src, 
                    'real_path' => $path, 
                    'type' => $this->galleryType
                ])
            );
        }
    }

    public function removeImage(Request $request){
        
        $image = Image::whereId($request->id)->first();
        $image->delete();

        return redirect()->back();
    }
}
