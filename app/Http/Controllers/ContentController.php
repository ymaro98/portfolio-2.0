<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function update(Request $request){
        $content = Content::first();
        $content->update($request->all());
        return redirect()->back();
    }
}
