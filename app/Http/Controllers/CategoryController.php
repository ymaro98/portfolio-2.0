<?php

namespace App\Http\Controllers;

use App\Models\ProjectTag;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public $tag_type_id;

    public function __construct()
    {
        $this->tag_type_id = 1;
    }

    public function index($category)
    {
        $category = ProjectTag::where('tag_type_id', $this->tag_type_id)->findOrFail($category);
        
        return Inertia::render('Frontend/Projects', [
            'projects' => $category->projects()
            ->latestWithRelations()
            ->get(),
        ]);
    }

    public function wbIndex()
    {
        $categories = ProjectTag::where('tag_type_id', $this->tag_type_id)->paginate(6);
        return Inertia::render('Backend/Categories/Index', [
            'categories' => $categories 
        ]);
    }

    public function wbShow($id){
        $category = ProjectTag::whereId($id)->where('tag_type_id', $this->tag_type_id)->first();
        return Inertia::render('Backend/Categories/Detail', [
            'category' => $category
        ]);
    }

    public function create(){
        $newProjectId = ProjectTag::all()->last()->id ?? 0;
        return Inertia::render('Backend/Categories/New', [
            'newProjectId' => $newProjectId
        ]);
    }

    public function store(Request $request){
        ProjectTag::create($request->all() + ['tag_type_id' => $this->tag_type_id]);
        return redirect()->back();
    }

    public function edit(Request $request){

        $category_id = $request->id;
        $category = ProjectTag::find($category_id)->where('tag_type_id', $this->tag_type_id)->get();
        $category->update($request->all());
 
        return redirect()->back();
    }

    public function delete(Request $request){

        $category_id = $request->id;
        $category = ProjectTag::find($category_id)->where('tag_type_id', $this->tag_type_id)->get();
        $category->delete();

        return redirect()->back();
    }
}
