<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\Image;
use App\Models\ProjectList;
use Illuminate\Database\Eloquent\Collection;
use Inertia\Inertia;

class ProjectController extends Controller
{
    public $tagController;

    public function __construct()
    {
        $this->tagController = new ProjectTagController;
    }

    public function index(){
        return Inertia::render('Frontend/Projects/Index');
    }

    public function show(){
        return Inertia::render('Frontend/Projects/Detail');
    }
    
    public function wbIndex(){
        $projects = Project::latest()->paginate(6);
        return Inertia::render('Backend/Projects/Index', [
            'projects' => $projects 
        ]);
    }

    public function wbShow($id){
        $project = Project::whereId($id)->with(['bgImage','galleryImages', 'tags'])->first();
        $projectTags = [
            'categories' => [
                "name" => 'categories',
                "items" => [],
            ],
			'technologies' => [
                "name" => 'technologies',
                "items" => [],
            ],
			'project_types' => [
                "name" => 'project_types',
                "items" => [],
            ],
        ];
        
        if($project->tags->count() > 0){
            foreach ($project->tags as $tag) {
                if($tag->tag_type_id == 1){
                    $type = 'categories';
                }
                if($tag->tag_type_id == 2){
                    $type = 'technologies';
                }
                if($tag->tag_type_id == 3){
                    $type = 'project_types';
                }
                
                array_push($projectTags[$type]['items'], ['text' => $tag->name, 'id' => $tag->id]);
            }
        }
        
        return Inertia::render('Backend/Projects/Detail', [
            'project' => $project,
            'tagsToLoad' => $projectTags,
        ]);
    }

    public function create(){
        $newProjectId = Project::all()->last()->id ?? 0;
        return Inertia::render('Backend/Projects/New', [
            'newProjectId' => $newProjectId
        ]);
    }

    public function featuredProjects($slug){
        $projectList = ProjectList::where('slug', $slug)->with('projects')->first();
        return $projectList;
    }

    public static function allProjects(){
        $projects = Project::latest()->isActive()->get();
        return $projects;
    }

    public function store(Request $request){
        $request->validate([
            'image' => 'required|file|image',
        ]);
        $project = Project::create($request->except('image'));

        $hasFile = $request->hasFile('image');
        if($hasFile){
            $image = new ImageController();
            $image->storeProjectImage($request, $project);
            $project->save();
        }

        return redirect()->back();
    }

    public function edit(Request $request){
        $project_id = $request->id;
        $project = Project::find($project_id);
        $project->update($request->except(['image', 'images', 'prefix', 'categories', 'technologies']));
        
        $allTags = new Collection;
        $categories = $request->categories;
        $technologies = $request->technologies;
        $projectTypes = $request->project_types;
        $allTags = $allTags->mergeRecursive($categories);
        $allTags = $allTags->mergeRecursive($technologies);
        $allTags = $allTags->mergeRecursive($projectTypes);
        
        $this->tagController->storeTags($project_id, $allTags);

        $hasBgImage = $request->hasFile('image');
        if($hasBgImage){
            $image = new ImageController();
            $image->storeProjectImage($request, $project);
        }

        $hasGalleryImages = $request->hasFile('images');
        if($hasGalleryImages){
            $image = new ImageController();
            $image->storeProjectImageMultiple($request, $project);
        }
        
        return redirect()->back();
    }

    public function delete(Request $request){

        $project_id = $request->id;
        $project = Project::find($project_id);

        $project->listItems()->delete();
        $project->taggables()->delete();

        $projectDeleted = $project->delete();
        if($projectDeleted){
            $project->images()->delete();
        }

        return redirect()->route('wb.project.index');
    }

    public function projectsTaggable(Request $request)
    {
        $projects_q = Project::withImagesAndTags();
        $categoryFilter = $request->get('categories');
        $technologyFilter = $request->get('technologies');
        $typeFilter = $request->get('types');
        
        $allFilters = [
            "categories" => [],
            "technologies" => [],
            "types" => [],
        ];
        
        if($categoryFilter !== null){
            array_push($allFilters["categories"] , $categoryFilter);
        }
        if($technologyFilter !== null){
            array_push($allFilters["technologies"], $technologyFilter);
        }
        if($typeFilter !== null){
            array_push($allFilters["types"], $typeFilter);
        }

        foreach($allFilters as $key => $filter){

            if($filter){
                $$key = explode(',', $filter[0]);
                $items = $$key;

                $projects_q->orWhereHas("taggables.tag", function($q) use ($items){
                    $q->whereIn('slug', $items);
                })->first();
            }
        }

        $projects = $projects_q->isActive()->paginate(6);
        return response()->json($projects);
    }
}
