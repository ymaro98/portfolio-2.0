<?php

namespace App\Http\Controllers;

use App\Models\TagType;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TagTypeController extends Controller
{
    public function wbIndex()
    {
        $tagTypes = TagType::latest()->paginate(6);
        return Inertia::render('Backend/TagTypes/Index', [
            'tagTypes' => $tagTypes 
        ]);
    }

    public function wbShow($id){
        $tagType = TagType::whereId($id)->first();
        return Inertia::render('Backend/TagTypes/Detail', [
            'tagType' => $tagType
        ]);
    }

    public function create(){
        $newTagTypeId = TagType::all()->last()->id ?? 0;
        return Inertia::render('Backend/TagTypes/New', [
            'newTagTypeId' => $newTagTypeId + 1 
        ]);
    }

    public function store(Request $request){

        TagType::create($request->all());
        return redirect()->back();
    }

    public function edit(Request $request){

        $tagType_id = $request->id;
        $tagType = TagType::find($tagType_id);
        $tagType->update($request->all());
 
        return redirect()->back();
    }

    public function delete(Request $request){

        $tagType_id = $request->id;
        $tagType = TagType::find($tagType_id);
        $tagType->delete();

        return redirect()->route('wb.tagtype.index');
    }

    public function allTypes(){
        $types = TagType::all();
        return $types;
    }
}
