<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectTaggable;
use App\Models\ProjectTag;
use App\Models\TagType;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProjectTagController extends Controller
{

    public function wbIndex($slug)
    {   
        $tags_q = new ProjectTag;
        $tags = $tags_q->whereTagTypeSlug($slug)->with('tagType')->latest()->paginate(6);
        $tagType = TagType::where('slug', $slug)->first();

        return Inertia::render('Backend/Tags/Index', [
            'tags' => $tags, 
            'tagType' => $tagType, 
        ]);
    }

    public function wbShow($slug, $id){

        $tag = ProjectTag::whereId($id)->with('tagType')->first();
        return Inertia::render('Backend/Tags/Detail', [
            'tag' => $tag
        ]);
    }

    public function create($slug){
        $newTag = ProjectTag::all()->last();
        if($newTag){
            $newTagId = $newTag->id;
        } else {
            $newTagId = 0;  
        }
        $tag_type = TagType::where('slug', $slug)->first();

        return Inertia::render('Backend/Tags/New', [
            'newTagId' => $newTagId + 1,
            'tag_type' => $tag_type,
        ]);
    }

    public function store(Request $request){
        $tag_type_id = $request->type_id;
        ProjectTag::create($request->except('type_id') + ['tag_type_id' => $tag_type_id]);
        return redirect()->back();
    }

    public function edit(Request $request){
        $tag_id = $request->id;
        // dump($tag_id);
        $tag = ProjectTag::find($tag_id);
        // dd($tag);
        $tag->update($request->all());
 
        return redirect()->back();
    }

    public function delete(Request $request){

        $tag_id = $request->id;
        $tag = ProjectTag::find($tag_id);
        $tag->delete();

        return redirect()->route('wb.tag.index');
    }

    public function allTags(){
        $categories = ProjectTag::whereTagType(1)->limit(6)->get();
        $technologies = ProjectTag::whereTagType(2)->limit(6)->get();
        $types = ProjectTag::whereTagType(3)->limit(6)->get();
        $tags = [
            'categories' => $categories,
            'technologies' => $technologies,
            'types' => $types
        ];
        return $tags;
    }

    public function storeTags($id, $items){
        $project = Project::where('id', $id)->first();
        $project->taggables()->delete();
        foreach ($items as $item) {
            $item = ProjectTaggable::create([
                "project_id" => $id,
                "tag_id" => $item['id']
            ]);
        }
    }

    public function tagsById($id){
        $tags = ProjectTag::whereTagType($id)->get();
        return $tags;
    }

    public function allCategoryTags(){
        $tags = ProjectTag::whereCategory()->get();
        return $tags;
    }

    public function allCategoryTagNames(){
        $tags = ProjectTag::whereCategory()->get()->pluck('name');
        return $tags;
    }

    public function allTechnologyTags(){
        $tags = ProjectTag::whereTechnology()->get();
        return $tags;
    }

    public function allTechnologyTagNames(){
        $tags = ProjectTag::whereTechnology()->get()->pluck('name');
        return $tags;
    }
}
