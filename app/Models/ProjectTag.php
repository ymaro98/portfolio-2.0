<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectTag extends Model
{
    use HasFactory;

    protected $fillable = [
        'tag_type_id',
        'name',
        'slug',
        'icon',
    ];

    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'taggables');
    }

    public function taggables()
    {
        return $this->hasMany(ProjectTaggable::class, 'tag_id');
    }

    public function tagType()
    {
        return $this->belongsTo(TagType::class, 'tag_type_id');
    }

    public function scopeWhereCategory($query)
    {
        return $query->where('tag_type_id', 1);
    }

    public function scopeWhereTechnology($query)
    {
        return $query->where('tag_type_id', 2);
    }

    public function scopeWhereProjectType($query)
    {
        return $query->where('tag_type_id', 3);
    }

    public function scopeWhereTagType($query, $id)
    {
        return $query->where('tag_type_id', $id);
    }

    public function scopeWhereTagTypeSlug($query, $slug)
    {
        return $query->whereHas("tagType", function($q) use ($slug){
            $q->where('slug', $slug);
        });
        // return $query->orWhereHas("tagType", function($q) use ($slug){
        //     $q->where('slug', $slug);
        // });
    }
}
