<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProjectListItem;
use App\Models\Project;

class ProjectList extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'title',
        'intro_text',
        'status',
    ];
    
    public function projects()
    {
        return $this->belongsToMany('App\Models\Project', 'project_list_items')->with(['bgImage', 'galleryImages'])->isActive();
    }

    public function listItems()
    {
        return $this->hasMany(ProjectListItem::class, 'project_list_id');
    }
}
