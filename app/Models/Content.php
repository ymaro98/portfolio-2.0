<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'home_intro_content',
        'home_intro_img',
        'about_me_title',
        'about_me_content',
        'about_me_img',
        'featured_list_id',
        'portfolio_title',
        'portfolio_intro',
    ];

    public function projectList()
    {
        return $this->belongsTo('App\Models\ProjectList', 'featured_list_id');
    }
}
