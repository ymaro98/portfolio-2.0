<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Image;
use App\Models\ProjectTag;
use App\Models\ProjectTaggable;
// use App\Traits\Taggable;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'prefix',
        'image',
        'description',
        'full_description',
        'link',
        'date',
        'message_top',
        'message_bottom',
        'status',
    ];

    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }
    
    public function bgImage()
    {
        return $this->image()->whereBg();
    }

    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function galleryImages()
    {
        return $this->images()->whereGallery();
    }

    public function scopeGetBgImage($query)
    {
        $query->images()
            ->WhereBg()
            ->get();
    }

    public function scopeIsActive($query)
    {
        return $query->where('status', 'active');
    }
        
    public function scopeGetGalleryImages($query)
    {
        $query->images()
            ->WhereGallery()
            ->get();
    }

    public function scopeLatestWithRelations(Builder $query)
    {
        return $query->latest()
            ->with('tags')
        ;
    }

    public function tags()
    {
        return $this->hasManyThrough(ProjectTag::class, ProjectTaggable::class, 'project_id', 'id', 'id', 'tag_id');
    }

    public function taggables()
    {
        return $this->hasMany(ProjectTaggable::class, 'project_id');
    }

    public function lists()
    {
        return $this->hasManyThrough(ProjectList::class, ProjectListItem::class, 'project_id', 'id', 'id', 'project_list_id');
    }

    public function listItems()
    {
        return $this->hasMany(ProjectListItem::class, 'project_id');
    }

    public function scopeWithTags()
    {
        return $this->with(['categories', 'technologies']);
    }

    public function categoryTags()
    {
        return $this->tags()->whereTagType(1);
    }
    
    public function technologyTags()
    {
        return $this->tags()->whereTagType(2);
    }
    
    public function projectTypeTags()
    {
        return $this->tags()->whereTagTypeSlug('accusamus-et-accusantium-non');
    }

    public function categoryTaggables()
    {
        return $this->taggables()->whereTagType(1);
    }
    
    public function technologyTaggables()
    {
        return $this->taggables()->whereTagType(2);
    }

    public function scopeWithImagesAndTags($query)
    {
        return $query->with(['bgImage','galleryImages', 'categoryTags', 'technologyTags']);
    }
}
