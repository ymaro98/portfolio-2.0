<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'path', 
        'real_path',
        'type',
    ];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function scopeWhereBg(Builder $query)
    {
        return $query->where('type', 'bg');
    }

    public function scopeWhereGallery(Builder $query)
    {
        return $query->where('type', 'gallery');
    }

    public function url()
    {
        return Storage::url($this->path);
    }
}
