<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectListItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_list_id',
        'project_id',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function list()
    {
        return $this->belongsTo(Project::class, 'project_list_id');
    }
}
