<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ProjectTaggable extends Model
{
    use HasFactory;

    protected $table = 'taggables'; 

    protected $fillable = [
        'project_id',
        'tag_id',
    ];

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function tag()
    {
        return $this->belongsTo(ProjectTag::class, 'tag_id');
    }

    public function scopeWhereTagType(Builder $query, $id)
    {
        return $query->whereHas('tag', function ($query) use ($id){
            return $query->where('tag_type_id', '=', $id);
        })->get();
    }
}
