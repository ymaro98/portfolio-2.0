<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'full_name',
        'email',
        'subject_id',
        'message',
        'status',
    ];

    protected $casts = [
        'created_at' => "datetime:d F Y - G:i",
    ];

    public function subject()
    {
        return $this->belongsTo('App\Models\ContactSubject');
    }

    public function scopeIsNew($query)
    {
        return $query->where('status', 'unread');
    }
}
