<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'meta_description',
        'meta_keywords',
        'status',
    ];

    public static function findBySlug($slug)
    {
        return static::where('slug', $slug)
            ->where('status', 'ACTIVE')
        ;
    }
}
