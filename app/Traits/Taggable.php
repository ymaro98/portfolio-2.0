<?php

namespace App\Traits;

use App\Models\ProjectList;
use App\Models\ProjectListItem;
use App\Models\ProjectTag;
use App\Models\ProjectTaggable;

trait Taggable
{

    protected static function bootTaggable()
    {
        static::updating(function ($model){
            $model->tags()->sync(static::findTagsInContent($model->content));
        });

        static::created(function ($model){
            $model->tags()->sync(static::findTagsInContent($model->content));
        });
    }

    public function tags()
    {
        return $this->hasManyThrough(ProjectTag::class, ProjectTaggable::class, 'project_id', 'id', 'id', 'tag_id');
    }

    public function lists()
    {
        return $this->hasManyThrough(ProjectList::class, ProjectListItem::class, 'project_id', 'id', 'id', 'project_list_id');
    }

    private static function findTagsInContent($content)
    {
        preg_match_all('/@([^@]+)@/m', $content, $tags);

        return ProjectTag::whereIn('name', $tags[1] ?? [])->get();
    }
}
