@component('mail::message')
# Introduction

{{ $contact->full_name }} sent you a message on {{ $contact->created_at }}.

@component('mail::button', ['url' => route('wb.contact.details', $contact->id)])
Read full message from {{ $contact->full_name }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
