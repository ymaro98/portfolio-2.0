<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <!-- Scripts -->
        @routes
        <script src="{{ mix('js/app.js') }}" defer></script>
        
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180" href="/storage/images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/storage/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/storage/images/favicon/favicon-16x16.png">
        <link rel="manifest" href="/storage/images/favicon/site.webmanifest">
        <link rel="mask-icon" href="/storage/images/favicon/safari-pinned-tab.svg" color="#3aafa9">
        <link rel="shortcut icon" href="/storage/images/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#3aafa9">
        <meta name="msapplication-config" content="/storage/images/favicon/browserconfig.xml">
        <meta name="theme-color" content="#3aafa9">
    </head>
    <body class="font-sans antialiased">
        @inertia

        {{-- @env ('local')
            <script src="http://localhost:8080/js/bundle.js"></script>
        @endenv --}}
         {{-- @if (app()->isLocal())
            <script src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
        @endif --}}
    </body>
</html>
