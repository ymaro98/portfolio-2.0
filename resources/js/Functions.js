export const convertToSlug = (text) =>{
    if(text){
        return text.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-');
    }
    return null;
}

export const isMobile = (width = 750) => {
    var maxWidth = width;
    return window.innerWidth < maxWidth
}

export const isDesktopXl = (width = 1440) => {
    var maxWidth = width;
    return window.innerWidth > maxWidth
}

export const pageAtTop = (num = 0) => {
    return window.scrollY == num;
}

export const scrollTo = (pos, num) => {
    window.scroll(pos, num);
}

export const scrollToTop = () => {
    window.scrollTo(0, 0);
}

export const scrollToBottom = (num = 0) => {
    window.scrollTo(window.scrollX, window.scrollY + num);
} 