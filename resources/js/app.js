require('./bootstrap');

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import Layout from './Layouts/Frontend';

import CKEditor from '@ckeditor/ckeditor5-vue';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faTimes, faEnvelope, faPhone, faInfoCircle, faPlus, faArrowLeft, faChevronUp, faChevronDown, faEdit, faInbox, faThumbtack} from '@fortawesome/free-solid-svg-icons';
import { faInstagram, faLinkedin, faFacebookF } from '@fortawesome/free-brands-svg-icons';

library.add(faBars, faTimes, faEnvelope, faPhone, faInfoCircle, faPlus, faArrowLeft, faChevronUp, faChevronDown, faEdit, faInbox, faThumbtack, faInstagram, faLinkedin, faFacebookF);

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} | ${appName}`,
    resolve: (name) => {
        const page = require(`./Pages/${name}.vue`).default
        page.layout = page.layout || Layout
        return page
    },
    setup({ el, app, props, plugin }) {
        return createApp({ render: () => h(app, props) })
            .use(plugin)
            .use(CKEditor)
            .mixin({ methods: { route } })
            .component('fa', FontAwesomeIcon)
            .mount(el);
    },
});

InertiaProgress.init({ color: '#4B5563' });
